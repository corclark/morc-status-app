package com.corywclark.morcstatus;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

/**
 * Created by coryclark on 4/17/15.
 */
public class FetchStatusTask extends AsyncTask<Void, Void, Void> {

    private final Context mContext;
    private final String LOG_TAG = FetchStatusTask.class.getSimpleName();

    public FetchStatusTask(Context context) {
        mContext = context;
    }

    private void getTrailDataFromJSON(String statusJSONString) throws JSONException {
        try {
            JSONArray statusJsonArray = new JSONArray(statusJSONString);

            Vector<ContentValues> cvVector = new Vector<ContentValues>(statusJsonArray.length());

            for (int i = 0; i < statusJsonArray.length(); i++) {
                JSONObject trail = statusJsonArray.getJSONObject(i);

                ContentValues statusValues = new ContentValues();

                statusValues.put(DataContract.TrailEntry.COLUMN_UID, trail.getInt(DataContract.TrailEntry.COLUMN_UID));
                statusValues.put(DataContract.TrailEntry.COLUMN_NAME, trail.getString(DataContract.TrailEntry.COLUMN_NAME));
                statusValues.put(DataContract.TrailEntry.COLUMN_CONDITION_LINK, trail.getString(DataContract.TrailEntry.COLUMN_CONDITION_LINK));
                statusValues.put(DataContract.TrailEntry.COLUMN_FORUM_LINK, trail.getString(DataContract.TrailEntry.COLUMN_FORUM_LINK));
                statusValues.put(DataContract.TrailEntry.COLUMN_STATUS, trail.getString(DataContract.TrailEntry.COLUMN_STATUS));
                statusValues.put(DataContract.TrailEntry.COLUMN_DESCRIPTION, trail.getString(DataContract.TrailEntry.COLUMN_DESCRIPTION));
                statusValues.put(DataContract.TrailEntry.COLUMN_LAST_UPDATED, trail.getString(DataContract.TrailEntry.COLUMN_LAST_UPDATED));
                statusValues.put(DataContract.TrailEntry.COLUMN_UPDATED_BY, trail.getString(DataContract.TrailEntry.COLUMN_UPDATED_BY));

                cvVector.add(statusValues);
            }

            if (cvVector.size() > 0) {
                ContentValues[] cvArray = new ContentValues[cvVector.size()];
                cvVector.toArray(cvArray);
                mContext.getContentResolver().bulkInsert(DataContract.TrailEntry.CONTENT_URI, cvArray);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String statusJsonString = null;



        try {
            final String STATUS_URL = "http://www.google.com";
            Uri builtUri = Uri.parse(STATUS_URL);

            URL url = new URL(builtUri.toString());

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                return null;
            }
//            statusJsonString = buffer.toString();
            statusJsonString = "[{\"TrailUID\":\"13038\",\"TrailName\":\"Battle Creek\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?13038\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?25\",\"Status\":\"Closed\",\"ConditionDescription\":\"If you were wondering.. No, Battle Creek is still closed.\",\"LatestUpdateTime\":\"2015-03-13T02:17:30Z\",\"LatestUpdateBy\":\"neno\"},{\"TrailUID\":\"21109\",\"TrailName\":\"Monticello\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?21109\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?87\",\"Status\":\"Melting Do Not Ride\",\"ConditionDescription\":\"Bertram should be considered CLOSED until the trail thaws and dries out for the summer season.\",\"LatestUpdateTime\":\"2015-03-06T15:10:49Z\",\"LatestUpdateBy\":\"JayT\"},{\"TrailUID\":\"39857\",\"TrailName\":\"Carver Lake\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?39857\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?103\",\"Status\":\"Closed\",\"ConditionDescription\":\"[COLOR=#141823][FONT=helvetica]The trail was inspected by City of Woodbury staff on Friday, April 3. Trail conditions are still too wet and damp to open the trail. It will remain closed through the weekend and if it stays dry is on track to open early next week. Hang in there....the dirt season will be here soon![/FONT][/COLOR]\",\"LatestUpdateTime\":\"2015-04-03T17:25:17Z\",\"LatestUpdateBy\":\"mtb_guy\"},{\"TrailUID\":\"44739\",\"TrailName\":\"Eagan Bike Park\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?44739\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?108\",\"Status\":\"Tacky\",\"ConditionDescription\":\"good enough to go for now\",\"LatestUpdateTime\":\"2014-08-31T22:16:42Z\",\"LatestUpdateBy\":\"cglasford\"},{\"TrailUID\":\"28476\",\"TrailName\":\"Elm Creek\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?28476\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?54\",\"Status\":\"Closed\",\"ConditionDescription\":\"Elm Creek Singletrack will remain [B]closed[/B] through the weekend. Trail inspection was done this afternoon and there's still too many areas that are soft and/or muddy. We'll take a look again soon. If mother nature continues to cooperate we should be riding soon. Thank you for your patience.\",\"LatestUpdateTime\":\"2015-04-03T21:46:07Z\",\"LatestUpdateBy\":\"JayT\"},{\"TrailUID\":\"9180\",\"TrailName\":\"Lebanon Hills\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?9180\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?24\",\"Status\":\"Closed\",\"ConditionDescription\":\"Closed for the spring thaw. Hope for warm and not too much rain and maybe we will have a short break. See everyone on the dirt!\",\"LatestUpdateTime\":\"2015-03-06T23:48:26Z\",\"LatestUpdateBy\":\"dave t\"},{\"TrailUID\":\"12195\",\"TrailName\":\"MN River Bottoms\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?12195\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?27\",\"Status\":\"Fat Tires\",\"ConditionDescription\":\"Rode last night and the trail was really nice with just a few wet spots east of 35 (I rode from 77 to Ferry and back). I'm not gutsy enough to be calling it perfect, but sure seems to be nice and I saw plenty of traffic that was fat and skinny. I'd say go get dirty... It was my first fat tire ride on dirt as I only rode fat during winter and hitting the sandy sections was a blast!\",\"LatestUpdateTime\":\"2015-04-02T18:49:51Z\",\"LatestUpdateBy\":\"Nitro199\"},{\"TrailUID\":\"21305\",\"TrailName\":\"Murphy Hanrehan\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?21305\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?29\",\"Status\":\"Closed\",\"ConditionDescription\":\"Murphy-Hanrehan is closed during the spring thaw. Closed after noon today (Fri. Mar. 6).\",\"LatestUpdateTime\":\"2015-03-06T15:13:15Z\",\"LatestUpdateBy\":\"JayT\"},{\"TrailUID\":\"12656\",\"TrailName\":\"Salem Hills\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?12656\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?42\",\"Status\":\"Closed\",\"ConditionDescription\":\"Still muddy in the wooded areas. I will have another report on Monday.\",\"LatestUpdateTime\":\"2015-04-01T18:34:10Z\",\"LatestUpdateBy\":\"Shawno\"},{\"TrailUID\":\"11512\",\"TrailName\":\"Terrace Oaks\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?11512\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?26\",\"Status\":\"Closed\",\"ConditionDescription\":\"Remains closed and gated. Maybe next week. Stay out.\",\"LatestUpdateTime\":\"2015-03-31T22:55:10Z\",\"LatestUpdateBy\":\"jeffgude\"},{\"TrailUID\":\"11411\",\"TrailName\":\"Theodore Wirth\",\"TrailConditionLink\":\"http://www.morcmtb.org/forums/showthread.php?11411\",\"TrailForumLink\":\"http://www.morcmtb.org/forums/forumdisplay.php?61\",\"Status\":\"Damp\",\"ConditionDescription\":\"See previous post. Testing switch to summer conditions.\",\"LatestUpdateTime\":\"2015-04-03T20:48:53Z\",\"LatestUpdateBy\":\"gopherhockey\"}] ";
            getTrailDataFromJSON(statusJsonString);

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        return null;
    }
}
