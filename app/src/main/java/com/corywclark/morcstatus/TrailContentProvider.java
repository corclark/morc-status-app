package com.corywclark.morcstatus;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * Created by coryclark on 4/1/15.
 */
public class TrailContentProvider extends ContentProvider {
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private TrailDBHelper mDBHelper;
    public static final int TRAIL = 100;
//    public static final int APPS_WITH_BU = 101;
//    public static final int APPS = 102;

    private static final SQLiteQueryBuilder sAppsQueryBuilder;

    static {
        sAppsQueryBuilder = new SQLiteQueryBuilder();
        sAppsQueryBuilder.setTables(DataContract.TrailEntry.TABLE_NAME);
    }
    private Cursor getApps(String[] projection, String selection, String[] selectionArgs, String sortOrder){

        return sAppsQueryBuilder.query(mDBHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
    }

    public static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = DataContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, DataContract.TRAIL_PATH, TRAIL);
//        matcher.addURI(authority, DataContract.TRAIL_PATH + "/*", APP);
        //matcher.addURI(authority, AppDataContract.APPS_PATH + "/bu=*", APPS_WITH_BU);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        mDBHelper = new TrailDBHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case TRAIL:
                return DataContract.TrailEntry.CONTENT_TYPE;
//            case APP:
//                return DataContract.TrailEntry.CONTENT_ITEM_TYPE;
//            case APPS_WITH_BU:
//                return DataContract.TrailEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown URI: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor returnCursor;

        switch (sUriMatcher.match(uri)) {
            case TRAIL:
                returnCursor = getApps(projection, null, null, sortOrder);
                break;
            default:
                returnCursor = getApps(projection, null, null, sortOrder);
                break;
//                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        returnCursor.setNotificationUri(getContext().getContentResolver(), uri);

        return returnCursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        if (selection == null) selection = "1";
        switch (match) {
            case TRAIL:
                rowsDeleted = db.delete(
                        DataContract.TrailEntry.TABLE_NAME, selection, selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;
        if (selection == null) selection = "1";
        switch (match) {
            case TRAIL:
                rowsUpdated = db.update(
                        DataContract.TrailEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case TRAIL: {
                long _id = db.insert(DataContract.TrailEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = DataContract.TrailEntry.buildAllTrailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }

            default: {
                long _id = db.insert(DataContract.TrailEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = DataContract.TrailEntry.buildAllTrailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
//                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int returnCount = 0;

        switch (match) {
            case TRAIL:
                db.beginTransaction();
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(DataContract.TrailEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                db.beginTransaction();
//                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(DataContract.TrailEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
        }
    }
}
