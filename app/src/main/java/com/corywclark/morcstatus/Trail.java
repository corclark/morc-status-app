package com.corywclark.morcstatus;

/**
 * Created by coryclark on 2/23/15.
 */
public class Trail {
    public String name;
    public String status;
    public String lastUpdated;
    public String description;
    public String uid;
    public String conditionLink;
    public String forumLink;
    public String updatedBy;
    public Boolean myTrail;
}
