package com.corywclark.morcstatus;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by coryclark on 4/1/15.
 */
public class DataContract {
    public static final String CONTENT_AUTHORITY = "com.corywclark.morcstatus";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String TRAIL_PATH = "trail";

    public static final class TrailEntry implements BaseColumns {
        public static final String TABLE_NAME = "trails";
        public static final String COLUMN_NAME = "TrailName";
        public static final String COLUMN_STATUS = "Status";
        public static final String COLUMN_LAST_UPDATED = "LatestUpdateTime";
        public static final String COLUMN_UID = "TrailUID";
        public static final String COLUMN_CONDITION_LINK = "TrailConditionLink";
        public static final String COLUMN_FORUM_LINK = "TrailForumLink";
        public static final String COLUMN_DESCRIPTION = "ConditionDescription";
        public static final String COLUMN_UPDATED_BY = "LatestUpdateBy";
        public static final String COLUMN_MY_TRAILS = "MyTrails";
        public static final String COLUMN_ANDROID_ID = "_id";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TRAIL_PATH).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TRAIL_PATH;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TRAIL_PATH;

        public static Uri buildAllTrailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class MyTrails implements BaseColumns {
        public static final String TABLE_NAME = "mytrails";
        public static final String COLUMN_UID = "TrailUID";
        public static final String COLUMN_MY_TRAILS = "MyTrails";
        public static final String COLUMN_ANDROID_ID = "_id";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TRAIL_PATH).build();

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TRAIL_PATH;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + TRAIL_PATH;

        public static Uri buildAllTrailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
