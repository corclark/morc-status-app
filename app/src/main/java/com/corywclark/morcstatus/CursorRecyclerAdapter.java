package com.corywclark.morcstatus;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by coryclark on 5/1/15.
 */
public class CursorRecyclerAdapter extends RecyclerView.Adapter<CursorRecyclerAdapter.ViewHolder> {

    CursorAdapter mCursorAdapter;
    Context mContext;
    int mLayout;

    public CursorRecyclerAdapter(Context context, Cursor c, int layout) {

        mContext = context;
        mLayout = layout;
        mCursorAdapter = new CursorAdapter(mContext, c, 0) {

            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                return LayoutInflater.from(mContext).inflate(mLayout, parent, false);
            }

            @Override
            public int getItemViewType(int position) {
                return super.getItemViewType(position);
            }

            @Override
            public void bindView(View view, Context context, final Cursor cursor) {
                TextView trailName = (TextView) view.findViewById(R.id.trailNameTextView);
                trailName.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataContract.TrailEntry.COLUMN_NAME)));

                TextView status = (TextView) view.findViewById(R.id.trailStatusTextView);
                status.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataContract.TrailEntry.COLUMN_STATUS)));

                View buColor = view.findViewById(R.id.trailStatusInd);
                buColor.setBackgroundColor(
                        getColorForTrailStatus(
                                cursor.getString(
                                        cursor.getColumnIndexOrThrow(DataContract.TrailEntry.COLUMN_STATUS))));

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = cursor.getString(cursor.getColumnIndexOrThrow(DataContract.TrailEntry.COLUMN_CONDITION_LINK));
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        mContext.startActivity(i);
                    }
                });
            }
        };
    }

    private int getColorForTrailStatus(String status) {
        switch (status) {
            case "Open":
                return mContext.getResources().getColor(R.color.status_green);
            case "Closed":
                return mContext.getResources().getColor(R.color.status_red);
            default: return mContext.getResources().getColor(R.color.status_yellow);
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public int getItemCount() {
        return mCursorAdapter.getCount();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        mCursorAdapter.getCursor().moveToPosition(position);
        mCursorAdapter.bindView(holder.itemView, mContext, mCursorAdapter.getCursor());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Cursor cursor = mCursorAdapter.getCursor();
        View v = mCursorAdapter.newView(mContext, cursor, parent);
        return new ViewHolder(v);
    }
}
