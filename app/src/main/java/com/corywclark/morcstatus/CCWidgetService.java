//package com.corywclark.morcstatus;
//
//import android.appwidget.AppWidgetManager;
//import android.content.Context;
//import android.content.Intent;
//import android.widget.RemoteViews;
//import android.widget.RemoteViewsService;
//
//import org.apache.http.client.ResponseHandler;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.BasicResponseHandler;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
//* Created by coryclark on 2/23/15.
//*/
//
//public class CCWidgetService extends RemoteViewsService {
//
//    @Override
//    public RemoteViewsFactory onGetViewFactory(Intent intent) {
//        return (new CCRemoteViewFactory(this.getApplicationContext(), intent));
//    }
//
//    class CCRemoteViewFactory implements RemoteViewsFactory {
//
//        private Context mContext;
//        private int mAppWidgetId;
//        private List<Trail> mWidgetData;
//
//        public CCRemoteViewFactory(Context context, Intent intent) {
//            mContext = context;
//            mAppWidgetId = intent.getIntExtra(
//                    AppWidgetManager.EXTRA_APPWIDGET_ID,
//                    AppWidgetManager.INVALID_APPWIDGET_ID);
//
//        }
//
//        @Override
//        public void onCreate(){
//            populateList();
//        }
//
//        @Override
//        public void onDestroy(){
//        }
//
//        public int getCount() {
//            return mWidgetData.size();
//        }
//
//        public RemoteViews getViewAt(int position) {
//            RemoteViews rv = new RemoteViews(mContext.getPackageName(),
//                    R.layout.trail_status_cell);
//
//            Trail currentTrail = mWidgetData.get(position);
//
//            rv.setTextViewText(R.id.trailNameTextView,currentTrail.name);
//            rv.setTextViewText(R.id.trailStatusTextView, currentTrail.status);
//            return rv;
//        }
//
//        public void populateList() {
//            DefaultHttpClient httpClient = new DefaultHttpClient();
//            HttpGet httpGet = new HttpGet("http://www.morcmtb.org/forums/trailconditions.php");
//            ResponseHandler<String> resHandler = new BasicResponseHandler();
//            String page = "nothing";
//            try {
//                page = httpClient.execute(httpGet, resHandler);
//            } catch (Exception e){
//                e.printStackTrace();
//            }
//
//            Document doc = Jsoup.parse(page);
//            mWidgetData = new ArrayList<Trail>();
//
//            Elements fullThings = new Elements();
//            for (Element element : doc.getElementsByClass("alt0")) {
//                fullThings.add(element.parent());
//            }
//
//            for (Element element : fullThings) {
//                Trail t = new Trail();
//                t.name = element.getElementsByClass("alt0").text();
//                t.status = element.getElementsByClass("alt2").text();
//                t.description = element.getElementsByClass("alt3").text();
//                t.lastUpdated = element.getElementsByClass("alt2").text();
//
//                mWidgetData.add(t);
//            }
//        }
//
//        public RemoteViews getLoadingView() {
//            return null;
//        }
//
//        public int getViewTypeCount() {
//            return 1;
//        }
//
//        public long getItemId(int position) {
//            return position;
//        }
//
//        public boolean hasStableIds() {
//            return true;
//        }
//
//        public void onDataSetChanged() {
//        }
//    }
//}
//
