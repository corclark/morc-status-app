package com.corywclark.morcstatus;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by coryclark on 4/1/15.
 */
public class TrailDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "morc";

    public TrailDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_TRAILS_TABLE = "CREATE TABLE " + DataContract.TrailEntry.TABLE_NAME + " (" +
                DataContract.TrailEntry.COLUMN_UID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE, " +
                DataContract.TrailEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                DataContract.TrailEntry.COLUMN_LAST_UPDATED + " INTEGER NOT NULL, " +
                DataContract.TrailEntry.COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                DataContract.TrailEntry.COLUMN_STATUS + " TEXT NOT NULL, " +
                DataContract.TrailEntry.COLUMN_CONDITION_LINK + " TEXT NOT NULL, " +
                DataContract.TrailEntry.COLUMN_FORUM_LINK + " TEXT NOT NULL, " +
                DataContract.TrailEntry.COLUMN_UPDATED_BY + " TEXT, " +
                DataContract.TrailEntry.COLUMN_MY_TRAILS + " BOOL, " +
                DataContract.TrailEntry.COLUMN_ANDROID_ID + " INTEGER" +
                ");";

        sqLiteDatabase.execSQL(SQL_CREATE_TRAILS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}

