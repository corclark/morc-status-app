package com.corywclark.morcstatus;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by coryclark on 4/1/15.
 */
public class MorcApp extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(
                                Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(
                                Stetho.defaultInspectorModulesProvider(this))
                        .build());
    }
}
