//package com.corywclark.morcstatus;
//
//import android.app.PendingIntent;
//import android.appwidget.AppWidgetManager;
//import android.appwidget.AppWidgetProvider;
//import android.content.Context;
//import android.content.Intent;
//import android.net.Uri;
//import android.widget.ListView;
//import android.widget.RemoteViews;
//
//public class TrailStatusWidget extends AppWidgetProvider {
//
//    private Context mContext;
//    private CCArrayAdapter mAdapter;
//    private ListView mListView;
//    private AppWidgetManager mManager;
//
//    @Override
//    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
//        mManager = appWidgetManager;
//        mContext = context;
//
//        for (int id : appWidgetIds) {
//
//            Intent intent = new Intent(context, CCWidgetService.class);
//            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
//            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
//            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.trail_status_widget);
//            rv.setRemoteAdapter(id, R.id.widgetListView, intent);
//            rv.setEmptyView(R.id.widgetListView, R.id.textView);
//
//            // Here we setup the a pending intent template. Individuals items of a collection
//            // cannot setup their own pending intents, instead, the collection as a whole can
//            // setup a pending intent template, and the individual items can set a fillInIntent
//            // to create unique before on an item to item basis.
//            Intent toastIntent = new Intent(context, CCWidgetService.class);
//            toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
//            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
//            PendingIntent toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent,
//                    PendingIntent.FLAG_UPDATE_CURRENT);
//            rv.setPendingIntentTemplate(R.id.widgetListView, toastPendingIntent);
//
//            appWidgetManager.updateAppWidget(id, rv);
//
////            Intent intent = new Intent(context, CCWidgetService.class);
////            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
////            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
////
////            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.trail_status_widget);
////            rv.setRemoteAdapter(id, intent);
////            rv.setEmptyView(R.id.widgetListView, R.id.textView);
////            appWidgetManager.updateAppWidget(id, rv);
//        }
//        super.onUpdate(mContext, appWidgetManager, appWidgetIds);
//    }
//
//    @Override
//    public void onEnabled(Context context) {
//        // Enter relevant functionality for when the first widget is created
//    }
//
//    @Override
//    public void onDisabled(Context context) {
//        // Enter relevant functionality for when the last widget is disabled
//    }
//
////    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
////                                int appWidgetId) {
////
////        CharSequence widgetText = context.getString(R.string.appwidget_text);
////        // Construct the RemoteViews object
////        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.trail_status_widget);
////
////        // Instruct the widget manager to update the widget
////        appWidgetManager.updateAppWidget(appWidgetId, views);
////    }
//}
//
//
