package com.corywclark.morcstatus;

import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

public class StatusListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mListView;
    private CursorRecyclerAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private static final String LOG_TAG = StatusListFragment.class.getSimpleName();
    private Toolbar mToolbar;
    private static final int TRAIL_STATUS_LOADER = 0;

    public StatusListFragment() {
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setEmptyText("No data loaded yet...");
        setHasOptionsMenu(false);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mAdapter == null) {
            refreshData();
        }
        View view = inflater.inflate(R.layout.fragment_statuslist, container, false);
        mToolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(Color.GREEN,
                Color.RED,
                Color.BLUE);

        mListView = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        mListView.setLayoutManager(llm);

        mListView.setOnScrollListener(new ScrollHideListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });

        return view;
    }

    public void refreshData(){
        FetchStatusTask task = new FetchStatusTask(getActivity());
        task.execute();
    }

    private void hideViews() {
        mToolbar.animate().translationY(-mToolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        mToolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String sortOrder = DataContract.TrailEntry.COLUMN_NAME + " ASC";
        Uri trailListUri = DataContract.TrailEntry.buildAllTrailsUri(TRAIL_STATUS_LOADER);

        return new CursorLoader(getActivity(),
                trailListUri,
                null,
                null,
                null,
                sortOrder);


    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (mAdapter == null) {
            mAdapter = new CursorRecyclerAdapter(getActivity(), data, R.layout.trail_status_cell);
        }
        Log.i(LOG_TAG, "onLoadFinished");
        Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
        mSwipeRefreshLayout.setRefreshing(false);
        mAdapter.mCursorAdapter.swapCursor(data);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader loader) {
        mAdapter.mCursorAdapter.swapCursor(null);
    }

    public void setEmptyText(CharSequence emptyText) {
    }

    @Override
    public void onRefresh() {
        new FetchStatusTask(getActivity()).execute();
    }
}
